import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import avion from './assets/plane.png';
import menu from './assets/menu.png';
import close from './assets/menu-close.png';

function Header() {
  function Logo () {
    return (
      <div className="logo"><img src={avion}/> TRAVEL AGENCY</div>
    )
  }
  function Menu() {
    const [isToggled, setToggle] = useState(false);
    let mobileMenu
    const handleClick = (e) => {
      setToggle(!isToggled);
      if(isToggled) {
        console.log('yes');
        mobileMenu = <nav className="mobile">
        <ul>
        <li><a className="active" href="index.html">Home</a></li>
        <li><a href="paris.html">Destination</a></li>
        <li>Deals</li>
        <li><a href="contact.html">Contact</a></li>
        </ul>
        </nav>
      } else {
        mobileMenu = '';
      }
    }
    return(
      <div className="div">
       <div className="menu"><img src={menu} onClick={handleClick}/></div>
       {mobileMenu}
      </div>
    
    );
  }

    return (
      <div className="Header">
                <header>
                <Logo/>
            <nav className="desktop">
                <ul>
                <li><Link to='/'>Home</Link></li>
                <li>Deals</li>
                <li><Link to='/contact'>Contact</Link></li>
                </ul>
            </nav>
            <Menu/>
            </header>
      </div>
    );
  }
  
  export default Header;
  