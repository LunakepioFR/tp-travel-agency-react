import React from 'react'
import Card from './Destination';

function Home() {
    return (
      <div className="Home">
       <div className="row">
      <div className="container">
        <div className="w-60">
          <div className="form">
            <div className="column-2">
              <div className="title"><h2>Find your holiday</h2></div>
              <div className="formulaire">
                <div className="column">
                  <label for="cityFrom">From :</label>
                  <input type="text" name="cityFrom" id="" />
                </div>
                <div className="column">
                  <label for="cityTo">To :</label>
                  <input type="text" name="cityTo" id="" />
                </div>
                <div className="column">
                  <label for="date">Date :</label>
                  <input type="date" name="where" id="" />
                </div>
                <div className="column">
                  <label for="duration">Duration :</label>
                  <input type="number" name="duration" id="" />
                </div>
              </div>
              <div className="confirm">
                <button type="submit">Find !</button>
              </div>
            </div>
          </div>
        </div>
        <div className="w-39">
          <div className="deal">
            <div className=""><h2>Deal of the day</h2></div>
            <div className="container">
              <div className="deal-info">
                <span className="dealTitle">Amsterdam</span>
                <ul>
                  <li>Hotel Calypso</li>
                  <li>1 week</li>
                </ul>
              </div>
              <div className="price-tag"><p>150 $</p></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <h3>Top destinations</h3>
    <div className="row">
      <div className="grid-container">
        <Card name="sydney"/>
        <Card name="rio"/>
        <Card name="cusco"/>
        <Card name="delhi"/>
        <Card name="hongkong"/>
        <Card name="katmandou"/>
        <Card name="tokyo"/>
        <Card name="lisbonne"/>
      </div>
    </div>
    <div className="row center">
      <button>See more</button>
    </div>
      </div>
    );
  }
  
  export default Home;
  