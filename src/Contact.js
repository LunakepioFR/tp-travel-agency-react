
function Contact() {
    return (
        <div className="contact">
            <div className="row"><div className="contact-banner preview"><h1>Contact</h1></div></div><div className="row">
            <div className="container">
                <div className="w-45">
                    <div className="form contact">
                        <div className="column-2">
                            <div className="formulaire">
                                <div className="column">
                                    <label for="firstName">Firstname :</label>
                                    <input type="text" name="firstName" id="" />
                                </div>
                                <div className="column">
                                    <label for="lastName">Lastname :</label>
                                    <input type="text" name="lastName" id="" />
                                </div>
                                <div className="column">
                                    <label for="date">Email :</label>
                                    <input type="email" name="email" id="" />
                                </div>
                                <div className="column">
                                    <label for="agency">Nearest agency :</label>
                                    <input type="select" name="duration" id="" />
                                </div>
                            </div>
                            <label for="message">Message :</label>
                            <textarea name="message"></textarea>
                            <div className="confirm">
                                <button type="submit">Send !</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="w-45">
                    <div className="text">
                        <h2>Address</h2>
                        <ul>
                            <li>15 rue de la paix</li>
                            <li>75010, Paris</li>
                            <li>France</li>
                            <li>+1 0805-540-801</li>
                            <li>Other addresses</li>
                        </ul>
                        <h2>Information</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. A maxime eum assumenda quidem fuga itaque laboriosam facere, facilis est mollitia explicabo beatae sunt deleniti nisi doloremque. Doloremque, ex nobis hic tempore asperiores sed quas debitis at illum eligendi, alias nisi reprehenderit voluptates, porro eaque quod aspernatur! Obcaecati deserunt in magnam, ex numquam accusantium officiis rerum soluta eius ut sunt voluptas! Accusamus obcaecati nemo ab perferendis vitae illo excepturi aperiam distinctio, officia perspiciatis, similique vel dolorum odit, mollitia repudiandae error fugit et culpa autem doloremque voluptate quas! Cupiditate tenetur molestiae tempore!</p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}

export default Contact;