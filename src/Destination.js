import React, { Component } from 'react';

class Card extends React.Component {

    render() {
        const cityName = this.props.name.charAt(0).toUpperCase() + this.props.name.slice(1)
        
      return( 
      <div className={`destination ${ this.props.name } `}>
        <div className=""><h2> {cityName} </h2></div>
        <div className="container">
          <div className="deal-info">
            <ul>
              <li>Hotel Calypso</li>
              <li>1 week</li>
            </ul>
          </div>
          <div className="price-tag"><p>150 $</p></div>
        </div>
      </div>);
    }
  }

  export default Card