import React from 'react'
import calculator from './assets/calculator.png';
import discount from './assets/discount.png';
import support from './assets/support.png';
import payment from './assets/online-payment.png';

function Footer() {

  function Test() {
    return (
      <ul>
          <li><div className="footer-menu"><img src={calculator}/><span>Compare prices</span> </div></li>
          <li><div className="footer-menu"><img src={discount}/><span>Get the best deals</span> </div></li>
          <li><div className="footer-menu"><img src={support}/><span>7/7 support</span> </div></li>
          <li><div className="footer-menu"><img src={payment}/><span>Secure online payment</span> </div></li>
        </ul>
    );
  }
    return (
      <div className="footer">
      <footer>
      <div id="footer-top">
      <Test/>
      </div>
     <div className="row">
      <div className="legal">
        <div className="w-33">
          <ul>
            <li><strong>Links</strong></li>
            <li><a href="index.html">Home</a></li>
            <li><a href="paris.html">Destination</a></li>
            <li>Deals</li>
            <li><a href="contact.html">Contact</a></li>
            <li><a href="terms-conditions.html">Terms & Conditions</a></li>
          </ul>
        </div>
        <div className="w-33">
          <ul>
            <li><strong>Address</strong></li>
            <li>15 rue de la paix</li>
            <li>75010, Paris</li>
            <li>France</li>
            <li>Other adresses</li>
          </ul>
        </div>
        <div className="w-33">
          <ul>
            <li><strong>Call Us</strong></li>
            <li>+1 0805-540-801</li>
          </ul>
        </div>
      </div>
     </div>
     </footer>
      </div>
    );
  }
  
  export default Footer;
  