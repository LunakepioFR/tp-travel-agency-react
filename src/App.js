import { Routes, Route } from 'react-router-dom';
import './App.scss';
import Contact from './Contact';
import Footer from './Footer';
import Header from './Header';
import Home from './Home';

function App() {
  return (
    <div className="App">
      <Header/>
      <Routes>
        <Route exact path="/" element={<Home/>}/>
        <Route exact path="/contact" element={<Contact/>}></Route>
      </Routes>

      <Footer/>
    </div>
  );
}

export default App;
